#!/bin/sh

here=$(pwd)

# path to compiled SparseConvNet binary
# scn_binary="/home/jiho/gdrive/projects/image_classification/ecotaxa_ml/scn/ecotaxa"
scn_binary="../scn/ecotaxa"

# input and output directories
export MODEL_DIR="$here/model_dir"
export OUTPUT_DIR="$here/output_dir"


## TRAIN ----

# input data
export TRAINING_DATA_FN="$here/input_dir/input_file_labeled.csv"
# start from scratch and train for a while
export EPOCH=0
export STOP_EPOCH=3
# dump features of the training set to compute PCA on them
export DUMP_FEATURES=1

./$scn_binary
# then train PCA on the features and store weights and trained PCA


## COMPUTE FEATURES ----
./pca.py train $OUTPUT_DIR/training_features.csv $MODEL_DIR/pca 

# input data
export UNLABELED_DATA_FN="$here/input_dir/input_file_unlabeled.csv"
# start from when the training stopped
# NB: this is the previous STOP_EPOCH - 1 because of 0-based indexing
export EPOCH=2
# stop early => do not train
export STOP_EPOCH=0
# dump features for later use
export DUMP_FEATURES=1

./$scn_binary
# then apply pre-trained PCA on features and store 30 components in db

./pca.py compress $OUTPUT_DIR/unlabeled_features.csv $MODEL_DIR/pca $OUTPUT_DIR/unlabeled_features_compressed.csv
./pca.py compress $OUTPUT_DIR/training_features.csv $MODEL_DIR/pca $OUTPUT_DIR/training_features_compressed.csv

