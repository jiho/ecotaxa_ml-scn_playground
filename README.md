# Test dataset for EcoTaxa <-> SparseConvNet communication

Run `input_dir/create_input_files.sh` to generate the input files with the appropriate paths.

Then check how environment variables are set in either the shell or python scripts `call_scn.**`