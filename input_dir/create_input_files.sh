#!/bin/bash
#
# Create input files 
#

# get absolute path
here=$(pwd)

i=1

find exportimg_343_20170921_1642 -name "*.jpg" | while read file
do
  # extract label from the file path
  label="${file%/*}"
  label="${label##*/}"
  # add line to each file
  echo "$i,$here/$file,$label" >> input_file_labeled.csv
  echo "$i,$here/$file" >> input_file_unlabeled.csv
  # increase counter
  i=$(expr $i + 1)
done

echo "Done"
