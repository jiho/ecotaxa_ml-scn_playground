#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 08:55:24 2017

@author: mschroeder, joirisson
"""

from subprocess import Popen, TimeoutExpired, DEVNULL, PIPE
from tempfile import mkstemp, mkdtemp
import os
import glob
from itertools import islice
import shutil

root = "/home/jiho/gdrive/projects/image_classification/ecotaxa_ml/playground/"

# location of compiled SparseConvNet binary
# scn_binary = "/home/jiho/gdrive/projects/image_classification/ecotaxa_ml/scn/ecotaxa"
scn_binary = root + "../ecotaxa"

# MODEL_DIR contains the weights (_epoch-X.cnn), and the class file (classes.txt) 
model_dir = root + "model_dir"
# OUTPUT_DIR is an existing temporary directory, where the results are put
# After running SCN, it will contain one or more of {training,validation,testing,unlabeled}_{predictions,confusion,features}.csv
output_dir = root + "output_dir"

## Training settings:
# SCN will train for a few epoch, based on a provided training file, then features of these images will be extracted to perform PCA
train_env = {
       "MODEL_DIR": model_dir,
       "OUTPUT_DIR": output_dir,
       # input data
       "TRAINING_DATA_FN": root + "input_dir/input_file_labeled.csv",
       # start from scratch and train for a while
       "EPOCH": "0",
       "STOP_EPOCH": "20", # typically 100
       # whether to compute features on the input file
       "DUMP_FEATURES": "1"
}

## Features dumping settings:
# SCN will read weights from a trained model and use them to compute features of images in an unlabeled dataset
features_env = {
       "MODEL_DIR": model_dir,
       "OUTPUT_DIR": output_dir,
       # input data
       "UNLABELED_DATA_FN": root + "input_dir/input_file_unlabeled.csv",
       # start at the last epoch (NB: this is the previous STOP_EPOCH - 1 because of 0-based indexing)
       "EPOCH": "19",
       # stop early = do not train
       "STOP_EPOCH": "0",
       # whether to compute features on the input file
       "DUMP_FEATURES": "1"
}


env = train_env
# env = features_env

# display the current environment
for k, v in env.items():
    print("{}: {}".format(k,v))

# time out for the communication with the binary
# beware, this is for the full execution event while execution can be long for training
timeout_in_s = 1200

# bufsize=1: Line buffering
with Popen(scn_binary, stdin=DEVNULL, stdout=PIPE, stderr=PIPE, env=env, universal_newlines=True, bufsize=1) as p:
    try:
        outs, errs = p.communicate(timeout=timeout_in_s)
    except TimeoutExpired:
        p.kill()
        outs, errs = p.communicate()

print("Return code: {}".format(p.returncode))
        
print("Output:")
print(outs)

print()
print("Errors:")
print(errs)

# # Read the features for the unlabeled dataset
# with open(os.path.join(output_dir, "unlabeled_features.csv"), "r") as f:
#     for line in f:
#         line = line.strip()
#         objid, label, *features = line.split(",")
        
        #print(objid, features)
        
# Remove output directory
# shutil.rmtree(output_dir)
        
# Remove input file
# os.unlink(unlabeled_data_fn)