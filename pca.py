#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 11:33:35 2017

@author: mschroeder
"""

import numpy as np
from sklearn.decomposition import PCA
from sklearn.externals import joblib

def load_features(features_fn):
    """
    Don't use np.loadtxt or np.genfromtxt, as these won't preallocate an array
    and instead assemble a list of values in an inefficient way.
    """
    
    delimiter=","
    
    with open(features_fn) as f:
        lines = list(f)
        
    n_cols = len(lines[0].rstrip("\n").split(delimiter))
    n_rows = len(lines)
    
    # Allocate an array of the right size
    dtype = np.dtype([("objid", "<U32"), ("y", np.int), ("X", np.float, n_cols - 2)])
    data = np.empty(n_rows, dtype)
    
    for i, line in enumerate(lines):
        objid, y, *X = line.rstrip("\n").split(delimiter)
        data["objid"][i] = objid
        data["y"][i] = int(y)
        data["X"][i] = np.array(X, np.float)
        
    return data

def do_train(args):
    # Load features
    print("Loading features...")
    features = load_features(args.features_fn)

    # Fit and save PCA
    print("Fitting PCA...")
    pca = PCA(n_components = args.pca_components)
    pca.fit(features["X"])
    joblib.dump(pca, args.pca_fn)
    
def do_compress(args):
    # Load features
    print("Loading features...")
    features = load_features(args.features_fn)

    # Fit and save PCA
    print("Fitting PCA...")
    pca = joblib.load(args.pca_fn)
    transformed_features = pca.transform(features["X"])
    
    with open(args.output_fn, "w") as f:
        for (objid, y), X in zip(features[["objid", "y"]], transformed_features):
            f.write(",".join([objid, str(y)] + [str(x) for x in X]) + "\n")

def main():
    from argparse import ArgumentParser
    
    parser = ArgumentParser(description="""
    Create a image collection based on a collection index.
    """)
    subparsers = parser.add_subparsers()
    
    parser_train = subparsers.add_parser('train', help="Train a PCA model")
    parser_train.add_argument("features_fn")
    parser_train.add_argument("pca_fn")
    parser_train.add_argument("--pca-components", type=int, default=50)
    parser_train.set_defaults(func=do_train)
    
    parser_compress = subparsers.add_parser('compress', help="Compress features")
    parser_compress.add_argument("features_fn")
    parser_compress.add_argument("pca_fn")
    parser_compress.add_argument("output_fn")
    parser_compress.set_defaults(func=do_compress)

    args = parser.parse_args()
    
    if args.func:
        args.func(args)
    else:
        parser.print_help()
    
if __name__ == "__main__":
    main()
